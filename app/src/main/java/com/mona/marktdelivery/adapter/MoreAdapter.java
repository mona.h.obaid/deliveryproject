package com.mona.marktdelivery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mona.marktdelivery.R;
import com.mona.marktdelivery.model.Item;

import java.util.ArrayList;

public class MoreAdapter  extends RecyclerView.Adapter<MoreAdapter.ViewHolder> {

    ArrayList<Item> data;
    private ItemClickListener mClickListener;
    private Context context;

    public MoreAdapter(ArrayList<Item> data, Context context) {
        this.data = data;
        this.context = context;
    }


    @NonNull
    @Override
    public MoreAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.more_layout, null, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull MoreAdapter.ViewHolder holder, int i) {
        int imageId = context.getResources().getIdentifier(data.get(i).getImageItem(), "drawable", context.getPackageName());
        holder.imgItem.setImageResource(imageId);
        holder.nameItem.setText(data.get(i).getNameItem());
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Item getItem(int i) {
        return data.get(i);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imgItem;
        public TextView nameItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItem = itemView.findViewById(R.id.imagItem);
            nameItem = itemView.findViewById(R.id.nameItem);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
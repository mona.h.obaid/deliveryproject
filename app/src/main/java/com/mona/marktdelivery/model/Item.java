package com.mona.marktdelivery.model;

public class Item {
    String imageItem;
    int nameItem;

    public Item(String imageItem, int nameItem) {
        this.imageItem = imageItem;
        this.nameItem = nameItem;
    }

    public String getImageItem() {
        return imageItem;
    }

    public void setImageItem(String imageItem) {
        this.imageItem = imageItem;
    }

    public int getNameItem() {
        return nameItem;
    }

    public void setNameItem(int nameItem) {
        this.nameItem = nameItem;
    }
}

package com.mona.marktdelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;

public class ActivityNewOrder extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);

        final EditText txtNewOrder = findViewById(R.id.txtNewOrder);
        final EditText txtLocationDetail = findViewById(R.id.txtLocationDetail);
        final Button btnSend = findViewById(R.id.btnSend);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty((txtNewOrder.getText().toString())) )

                    {
                        Toast.makeText(ActivityNewOrder.this, "Enter your order", Toast.LENGTH_SHORT).show();
                    }

                else {

                    Toast.makeText(ActivityNewOrder.this, "ok", Toast.LENGTH_SHORT).show();
                }

            }
//
        });


    }


}

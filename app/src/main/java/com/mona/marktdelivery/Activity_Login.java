package com.mona.marktdelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;

public class Activity_Login extends AppCompatActivity implements CountryCodePicker.OnCountryChangeListener   {

    private CountryCodePicker  ccp=null;
    private String countryCode=null;
    private String countryName=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__login);

        ccp = findViewById(R.id.country_code_picker);
        ccp.setOnCountryChangeListener(this);
        ccp.setDefaultCountryUsingNameCode("PS");

        final EditText txtMobile=findViewById(R.id.txtPhone);
        final EditText txtPass=findViewById(R.id.txtPass);
        final Button btnDone=findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty((txtMobile.getText().toString())) || TextUtils.isEmpty((txtPass.getText().toString())) )
                {

                    if (TextUtils.isEmpty((txtMobile.getText().toString())))
                    {
                        txtMobile.setError("jjj");
                    }

                    if (TextUtils.isEmpty((txtPass.getText().toString())))
                    {
                        txtPass.setError("jjj");
                    }

                    Toast.makeText(Activity_Login.this, "error", Toast.LENGTH_SHORT).show();

                }else {

                    Toast.makeText(Activity_Login.this, "ok", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public void onCountrySelected() {
        countryCode=ccp.getSelectedCountryCode();
        countryName=ccp.getSelectedCountryName();

        Toast.makeText(this,"Country Code "+countryCode,Toast.LENGTH_SHORT).show();
        Toast.makeText(this,"Country Name "+countryName,Toast.LENGTH_SHORT).show();
    }
}

package com.mona.marktdelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;

public class ActivitySignupInfo extends AppCompatActivity  implements CountryCodePicker.OnCountryChangeListener{
    private CountryCodePicker  ccp=null;
    private String countryCode=null;
    private String countryName=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_info);

        ccp = findViewById(R.id.country_code_picker);
        ccp.setOnCountryChangeListener(this);
        ccp.setDefaultCountryUsingNameCode("PS");


        final EditText txtMobile=findViewById(R.id.txtMobileInfo);
        final EditText txtPss=findViewById(R.id.txtPassInfo);
        final EditText txtConfPss=findViewById(R.id.txtConPassInfo);
        final EditText txtName=findViewById(R.id.txtNameInfo);
        final EditText txtEmail=findViewById(R.id.txtemailInfo);

        final Button btnDone=findViewById(R.id.btnDoneInfo);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty((txtMobile.getText().toString())) || TextUtils.isEmpty((txtPss.getText().toString()))
                    || TextUtils.isEmpty((txtConfPss.getText().toString())) || TextUtils.isEmpty((txtName.getText().toString()))
                    || TextUtils.isEmpty((txtEmail.getText().toString()))
                )
                {

                    if (TextUtils.isEmpty((txtMobile.getText().toString())))
                    {
                        txtMobile.setError(getString(R.string.messg_mobile));
                    }

                    if (TextUtils.isEmpty((txtPss.getText().toString())))
                    {
                        txtPss.setError(getString(R.string.messg_pass));
                    }

                    if (TextUtils.isEmpty((txtConfPss.getText().toString())))
                    {
                        txtConfPss.setError(getString(R.string.messg_Confpass));
                    }

                    if (TextUtils.isEmpty((txtName.getText().toString())))
                    {
                        txtName.setError(getString(R.string.messg_name));
                    }

                    if (TextUtils.isEmpty((txtEmail.getText().toString())))
                    {
                        txtEmail.setError(getString(R.string.messg_email));
                    }
                    Toast.makeText(ActivitySignupInfo.this, getString(R.string.messg_allfield), Toast.LENGTH_SHORT).show();

                }else {

                    Toast.makeText(ActivitySignupInfo.this, "ok", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onCountrySelected() {
        countryCode=ccp.getSelectedCountryCode();
        countryName=ccp.getSelectedCountryName();

        Toast.makeText(this,"Country Code "+countryCode,Toast.LENGTH_SHORT).show();
        Toast.makeText(this,"Country Name "+countryName,Toast.LENGTH_SHORT).show();

    }
}

package com.mona.marktdelivery.fragment;


import android.content.res.TypedArray;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.mona.marktdelivery.R;
import com.mona.marktdelivery.adapter.MoreAdapter;
import com.mona.marktdelivery.model.Item;

import java.sql.Date;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment {

     ArrayList<Item> data=new ArrayList<>();
     RecyclerView recyclerView;
    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final    View root=inflater.inflate(R.layout.fragment_more, container, false);



        //------------------------------------------ inset All data -----------------------

        data.add(new Item("ic_language",R.string.txt_language));
        data.add(new Item("ic_profile",R.string.txt_profile));
        data.add(new Item("ic_oreders",R.string.txt_orders));
        data.add(new Item("ic_notifications",R.string.txt_notifications));
        data.add(new Item("ic_share",R.string.txt_share));
        data.add(new Item("ic_signout",R.string.txt_signout));


        //-------------------------------- GridView and custom Adapter --------------------
        final MoreAdapter moreAdapter=new MoreAdapter(data,getContext());
        RecyclerView.LayoutManager managerGrid=new GridLayoutManager(getContext(),2);
        recyclerView=root.findViewById(R.id.rvItem);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL));
        recyclerView.setLayoutManager(managerGrid);
        recyclerView.setAdapter(moreAdapter);
        return root;
    }

}

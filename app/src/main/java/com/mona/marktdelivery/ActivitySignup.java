package com.mona.marktdelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;

public class ActivitySignup extends AppCompatActivity implements CountryCodePicker.OnCountryChangeListener {


    private CountryCodePicker  ccp=null;
    private String countryCode=null;
    private String countryName=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ccp = findViewById(R.id.country_code_picker);
        ccp.setOnCountryChangeListener(this);
        ccp.setDefaultCountryUsingNameCode("PS");

        final EditText txtMobile=findViewById(R.id.txtMobileSingup);
        final EditText txtCode=findViewById(R.id.txtCodeSingup);
        final Button btnDone=findViewById(R.id.btnSingup);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty((txtMobile.getText().toString())) || TextUtils.isEmpty((txtCode.getText().toString())) )
                {

                    if (TextUtils.isEmpty((txtMobile.getText().toString())))
                    {
                        txtMobile.setError(getString(R.string.messg_mobile));
                    }

                    if (TextUtils.isEmpty((txtCode.getText().toString())))
                    {
                        txtCode.setError(getString(R.string.messg_Code));
                    }
                    Toast.makeText(ActivitySignup.this, getString(R.string.messg_allfield), Toast.LENGTH_SHORT).show();

                }else {

                    Toast.makeText(ActivitySignup.this, "ok", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onCountrySelected() {
        countryCode=ccp.getSelectedCountryCode();
        countryName=ccp.getSelectedCountryName();

        Toast.makeText(this,"Country Code "+countryCode,Toast.LENGTH_SHORT).show();
        Toast.makeText(this,"Country Name "+countryName,Toast.LENGTH_SHORT).show();

    }
}
